# Software Studio 2018 Spring Midterm Project

## Topic
* Forum

* Key functions
    1. user page
    2. post page
    3. post list page
    4. leave comment under any post
* Other functions
    1. delete post
    2. edit post
    3. history post list
    4. email verify

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description   
```website address:```https://software-studio-2018-chia.firebaseapp.com/index.html   
**1.(sign up/log in page)**   
進入主畫面後，使用者需要登入，才能獲取論壇中的資訊，若是新的使用者，可以選取sign up來註冊。可使用facebook和google來登入或註冊。也可以使用信箱註冊，此時須多輸入一個使用者名稱，另外需要多等1.5秒讓資料存入database，故這個延遲是正常現象。     
![](https://i.imgur.com/VasuLo8.jpg)
* 若沒有登入，只會看到這張圖和顯示log in to get more information.

**2.(post list page)**   
登入以及註冊後，便會以會員身分進入主畫面，可以在畫面上看見文章列表。若使用者是使用Google註冊或登入，同時會取得信箱認證，便可直接擁有發文權限，可點擊右下角的加來寫文章。若非google登入，則在進入profile並完成信箱認證前，都不會有發文權限。點擊加後會進入發文頁面，需要標題以及內容都非空白才能成功送出文章，送出文章前約會有1.5秒的延遲，是用來给將資料存入Database的時間，為正常現象。   
**3.(user page)**   
登入後，點擊右上角自己的名字，並進入Profile，可看到自己的user name、user email以及是否信箱驗證，若未信箱驗證，可以在這邊選擇驗證。上方會有自己的大頭貼，若是使用信箱註冊，在尚未上傳照片前，會先以下圖代替。若是使用facebook或google登入，會直接使用該大頭貼作為user page的大頭貼，可選擇上傳圖片來更改自己的大頭貼。    
![](https://i.imgur.com/ggLpnNX.jpg)    
**4.(history post list)**   
若是進入_History，則會列出自己所有之前發過的文章。方便使用者管理自己所發表的文章。  
**5.(post page)**   
點擊有興趣的標題，進去該文章的頁面。可在左邊看到發文者的名稱以及大頭貼，右邊上方看見文章標題以及發文時間，右邊下方為文章內容。   
**6.(delete post and edit post)**   
在右上角會有delete和edit，可以刪除文章以及編輯文章，刪除文章要點擊提醒視窗的確定，才算完成刪除，編輯也需要在編輯後submit才算完成編輯，若是選擇cancel則會維持原狀。   
**7.(leave comment under any post)**   
看完文章後如果想留言，可以在下方空格中打入內容並送出，便可以留言在文章下方。  
**8.(Chrome Notification)**    
當別人在你所發文的底下留言，且你剛好在線上，則會跳出一個通知，提醒你有人在你的發文底下留言，若點擊提醒視窗，則會跳到該文章頁面，也可以直接選擇關閉提醒。   
**9.(CSS animation)**   
進入主畫面、文章、文章編輯頁面，在資料讀取未完成時，會顯示css動畫，直到讀取完成。另外，左上角的小鬼圖，也會一直上下來回跳動。    
**10(Storage and Database)**
發表文章、留言這些都會將所需資料存進Database，而大頭貼則會上傳到Storage存放，所以從這兩個地方讀取的資料，都會需要一些loading的時間。    
**11(Email verify)**    
信箱驗證方式為：點擊左上方自己的user name進入Profile後，點擊自己emailVerify，這時候系統會寄一封信到你的信箱，點擊信件內的連結，再回到論壇，重整後就會發現已完成認證。  
## Security Report (Optional)
1. 使用者需要登入後，才能看到論壇中的文章以及其他資訊。   
2. 使用者需要經過信箱認證才可以發文，若未經過信箱認證，只可瀏覽文章以及在下方留言，不可發表文章。
3. 若有有心人士，想透過手動輸入網址，修改非自己發的文章，會透過一進入網站的偵測，將他送回首頁，不會讓他得逞。
4. 若有有心人士，想透過html的語法在內文、標題或留言中，試圖用特定的tag更改網頁(如：鑲嵌音樂在背景撥放)，已將這個方式鎖住。
