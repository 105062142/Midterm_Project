function init()
{
    
    firebase.auth().onAuthStateChanged(function(user) {

        if (user) {
            document.getElementById('user').innerHTML = "<span> user : </span>" + "<strong>" + user.displayName + "</strong>";
            document.getElementById('email').innerHTML = "<span> user email : </span>" + "<strong>" + user.email + "</strong>";
            document.getElementById('picture').src = user.photoURL;
            if(user.emailVerified) document.getElementById('btnverify').style.display = "none";
            document.getElementById('ver').innerHTML ="<span> email verified : </span>" + "<strong>" + user.emailVerified + "</strong>";
            var imgg = document.getElementById('pic');

            imgg.addEventListener('change',function(){
                var file = this.files[0];
                var storageRef = firebase.storage().ref();
                var uploadTask = storageRef.child('images/'+file.name).put(file);
                uploadTask.on('state_changed', function(snapshot){
                }, function(error) {
            }, function() {
                var downloadURL = uploadTask.snapshot.downloadURL;
                user.updateProfile({
                    photoURL : downloadURL
                });
                setTimeout(function(){
                    location.reload();
                }, 1000);
              });
            },false);
             var btnVerify = document.getElementById('btnverify');
             btnVerify.addEventListener("click",function(){
                 user.sendEmailVerification().then(function() {
                 console.log("驗證信寄出");
                 }, function(error) {
                 console.error("驗證信錯誤");
                 });
             },false);
            
        } 
        else {
            
        }
      });
      document.getElementById('picture').src = user.photoURL;
      notifyMe();
}

window.onload = function () {
    init();
    
};