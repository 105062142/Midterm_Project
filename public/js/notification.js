function notifyMe() {
    
    var first_count = 0;
    var second_count = 0;

    if(Notification.permission !== "granted")
        Notification.requestPermission();
    else
    {
        var postsRef = firebase.database().ref('com_list');
        postsRef.once('value').then(function(snapshot){
            snapshot.forEach(function(childSnapshot){
                var key = childSnapshot.key;
                var commentRef = firebase.database().ref('com_list/'+key+'/comment');
                commentRef.once('value').then(function(commentSnapshot){
                    commentSnapshot.forEach(function(){
                        first_count += 1;
                    });
                    commentRef.on('child_added',function(shot){
                        second_count += 1;
                        if(second_count > first_count)
                        {
                            if(childSnapshot.val().email == firebase.auth().currentUser.email)
                            {
                                var notification = new Notification('New comment!', {
                                    icon: 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
                                    body: shot.val().email + " left a new comment under your post!",
                                });
                        
                                notification.onclick = function () {
                                    window.open("show.html?" + key);
                                    notification.close();      
                                };
                            }   
                        }   
                    });
                });
            });
        });
    }
}
  


 
 
  