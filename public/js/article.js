function init()
{
    post_btn = document.getElementById('post_btn');
    post_title = document.getElementById('title');
    post_txt = document.getElementById('content');
    cancel_btn = document.getElementById('cancel_btn');
    var user;
    
    post_btn.addEventListener('click', function () {
        user = firebase.auth().currentUser;
        var nowtime = new Date();
        var month = ((nowtime.getMonth()+1) <10)? '0':'';
        var day = (nowtime.getDate() <10)? '0':'';
        var hour = (nowtime.getHours() <10)? '0':'';
        var min = (nowtime.getMinutes() <10)? '0':'';
        var posttime = nowtime.getFullYear() + "-" + month + (nowtime.getMonth()+1) + "-" + day + nowtime.getDate() + " " + hour + nowtime.getHours() + ":" + min + nowtime.getMinutes();
        if (post_txt.value != "" && post_title.value != "") 
        {
            var ref = firebase.database().ref('com_list');
            var data = {
                email: user.email,
                title: post_title.value,
                data: post_txt.value,
                user: user.displayName,
                nowtime:posttime,
                photo:user.photoURL
            };
            ref.push(data);
            post_title.value = "";
            post_txt.value = "";
            setTimeout(function(){
                window.location.href = "index.html";
            }, 1500); 
        }
    });

    cancel_btn.addEventListener('click', function () {
        window.location.href = "index.html";
    });
    notifyMe();
}

window.onload = function () {
    init();
};