function init()
{
    var url = location.href;
    var ary = url.split('?')[1].split('&');
    var link = "com_list/" + ary;

    var ref = firebase.database().ref(link);


    ref.once('value').then(function (snapshot) {
        if(firebase.auth().currentUser.email != snapshot.val().email) window.location.href = "index.html";
        document.getElementById('title').value =snapshot.val().title;
        document.getElementById('content').innerHTML = snapshot.val().data;
    }).then(e => document.getElementById('test').style.display = "none");

    post_btn = document.getElementById('post_btn');
    post_title = document.getElementById('title');
    post_txt = document.getElementById('content');
    cancel_btn = document.getElementById('cancel_btn');

    post_btn.addEventListener('click',function(){
        if (post_txt.value != "" && post_title.value != "") 
        {
            firebase.database().ref(link).update({
                    title: post_title.value,
                    data: post_txt.value
            }).then(e =>window.location.href = "show.html?" + ary); 
        }
    });

    cancel_btn.addEventListener('click', function () {
        window.location.href = "show.html?" + ary;
    });

    notifyMe();
}

window.onload = function () {
    init();
};