function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        // Check user login
        if (user) {
            user_email = user.email;
            var user = firebase.auth().currentUser;
            if(!user.emailVerified) document.getElementById('writearti').style.display = "none";
            
            document.getElementById('login').innerHTML = "<div class='dropdown'><button class='btn btn-dark dropdown-toggle' type='button' " +  
            "id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>" + user.displayName + "</button>" + 
            "<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>" + "<a class = 'dropdown-item' id='profile-btn' href = 'profile.html'>" + "Profile" + "</a>" +
            "<a class = 'dropdown-item' id='history-btn' href = 'history.html'>" + "_History Post" + "</a>" +
            "</div></div>";
            document.getElementById('signup').innerHTML = "<span class = 'nav-link active' id='logout-btn'>Logout</span>";
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(e => {
                    window.location.href = 'index.html';
                }).catch(e => {
                    alert("error");
                });
            });
            document.getElementById('writearti').style.visibility = "visible";
            document.getElementById('cov').style.display = "none";
        } 
        else {
            // It won't show any post if not login
            document.getElementById('login').innerHTML = "<a class='nav-link active' href='signin.html'>Login</a>";
            document.getElementById('signup').innerHTML = "<a class = 'nav-link active' href = 'signup.html'>Sign up</a>";
            document.getElementById('post_list').innerHTML = "";
            document.getElementById('writearti').style.visibility = "hidden";
            document.getElementById('test').style.display = "none";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    

    var str_before_username = "<div class='my-3 pt-3 rounded box-shadow text-muted media'>";
    var str_after_content = "</div>\n";
    var postsRef = firebase.database().ref('com_list');
    var total_post = [];
    var first_count = 0; 
    var second_count = 0;

    var np = 0;
    var arr = [];

    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot){
            var childData = childSnapshot.val();
            var uuuser = firebase.auth().currentUser;
            var temp = np.toString();
            total_post[total_post.length] = str_before_username + 
            "<a class='media-body pb-3 mb-0  lh-125' href = " + 'show.html' + '?' + childSnapshot.key + "><strong class='d-block'" + "<span id=" + temp + "></span>" + "</strong></a>" + childData.user + " " + childData.nowtime +
            str_after_content;
            first_count += 1;
            arr[arr.length]=childData.title;
            np += 1;
            });
            total_post.reverse();
            document.getElementById('post_list').innerHTML = total_post.join('');
            for(i = 0 ; i < np ; i++)
            {
                var temp = i.toString();
                document.getElementById(temp).innerText=arr[i];
            }

            postsRef.on('child_added',function(data){
                second_count += 1;
                var temp = np.toString();
                if(second_count > first_count){
                    var childData = data.val();
                    total_post.reverse();
                    total_post[total_post.length] = str_before_username + 
                    "<a class='media-body pb-3 mb-0  lh-125' href = " + 'show.html' + '?' + childSnapshot.key + "><strong class='d-block'>" + "<span id=" + temp + "></span>" + "</strong></a>" + childData.user + " " + childData.nowtime + 
                    str_after_content;
                    total_post.reverse();
                    document.getElementById('post_list').innerHTML = 
                    total_post.join('');
                    arr[arr.length] = childData.title;
                    np += 1;
                    for(i = 0 ; i < np ; i++)
                    {
                        var temp = i.toString();
                        document.getElementById(temp).innerText = arr[i];
                    
                    }
                }
            })
         }).then(e => document.getElementById('test').style.display = "none")
        .catch(e => console.log(e.message));

    
        notifyMe();
}

window.onload = function () {
    init();
};