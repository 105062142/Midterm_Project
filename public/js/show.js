function init()
{
   
    var url = location.href;
    var ary = url.split('?')[1].split('&');
    var link = "com_list/" + ary;

    var ref = firebase.database().ref(link);

    ref.once('value').then(function (snapshot) {
        document.getElementById('user').innerHTML = snapshot.val().user;
        document.getElementById('title').innerHTML ="<br><br><strong>" + "<span id = 'tittt'></span>" + "</strong>" + "<span style = 'float:right;'>" + snapshot.val().nowtime + "</span>";
        document.getElementById('tittt').innerText = snapshot.val().title;
        document.getElementById('content').innerText = snapshot.val().data;
        document.getElementById('writerimg').src = snapshot.val().photo;
        if(snapshot.val().email != firebase.auth().currentUser.email)
        {
            document.getElementById('deletearti').style.display = "none";
            document.getElementById('editarti').style.display = "none";
        }
        
    });

    var deletearti = document.getElementById('deletearti');

    deletearti.addEventListener('click', function(){
        if (window.confirm('Are you sure?') == true)
        {
            firebase.database().ref(link).remove().then(function(){
                window.location.href = "index.html";
            });
        }
    });

    var editarti = document.getElementById('editarti');

    editarti.addEventListener('click',function(){
        window.location.href = "edit.html?" + ary;
    });


    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    var comment = link + "/comment";
    

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var ref = firebase.database().ref(comment);
            var nowtime = new Date();
            var month = ((nowtime.getMonth()+1) <10)? '0':'';
            var day = (nowtime.getDate() <10)? '0':'';
            var hour = (nowtime.getHours() <10)? '0':'';
            var min = (nowtime.getMinutes() <10)? '0':'';
            var posttime = nowtime.getFullYear() + "-" + month + (nowtime.getMonth()+1) + "-" + day + nowtime.getDate() + " " + hour + nowtime.getHours() + ":" + min + nowtime.getMinutes();
            var data = {
                photo:firebase.auth().currentUser.photoURL,
                email: firebase.auth().currentUser.displayName,
                data: post_txt.value,
                nowtime:posttime
            };
            ref.push(data);
            post_txt.value = "";

        }
    });

   
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'>";
    var str_after_content = "</div>\n";
    var postsRef = firebase.database().ref(comment);
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    var np = 0;
    var arr = [];

    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot){
            var childData = childSnapshot.val();
            var temp = np.toString();
            total_post[total_post.length] = str_before_username + "<h6 class='media-body pb-3 mb-0 small lh-125'><strong>" + "<img src=" + childData.photo +  " alt='' class='mr-2 rounded' style='height:32px; width:32px;'>" +
            childData.email + "</strong>" + " (" + childData.nowtime + ")" + "</h6>"  + "<span id=" + temp + "></span>" +
            str_after_content;
            first_count += 1;
            arr[arr.length]=childData.data;
            np += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');
            for(i = 0 ; i < np ; i++)
            {
                var temp = i.toString();
                document.getElementById(temp).innerText=arr[i];
            }
            
            postsRef.on('child_added',function(data){
                second_count += 1;
                var temp = np.toString();
                if(second_count > first_count){
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username + "<h6 class='media-body pb-3 mb-0 small lh-125'><strong>" + "<img src=" + childData.photo +  " alt='' class='mr-2 rounded' style='height:32px; width:32px;'>" +
                    childData.email + "</strong>" + " (" + childData.nowtime + ")" + "</h6>"  + "<span id=" + temp + "></span>" +
                    str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                    arr[arr.length] = childData.data;
                    np += 1;
                    for(i = 0 ; i < np ; i++)
                    {
                        var temp = i.toString();
                        document.getElementById(temp).innerText = arr[i];
                    
                    }
                }
            })
         }).then(e => document.getElementById('test').style.display = "none");

        


        notifyMe();

}


window.onload = function () {
    init();
};