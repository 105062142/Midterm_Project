function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnFacebook = document.getElementById('btnfacebook');

    btnLogin.addEventListener('click', e =>  {
        var email = inputEmail.value;
        var password = inputPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(e => {
            window.location.href="index.html";
        }).catch(e => {
            create_alert('error',e.message);
            document.getElementById('inputEmail').value = '';
            document.getElementById('inputPassword').value = '';
        });
        
    });

    // Login with Google
    btnGoogle.addEventListener('click', e => {
        console.log('signInWithPopup');
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location.href="index.html";
        }).catch(function (error) {
        create_alert('error',error.message);
    });
    });

    // Login with Facebook
    btnFacebook.addEventListener('click', e => {
        console.log('signInWithPopup');
        var facebook_provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(facebook_provider).then(function (result) {
            console.log('signInWithPopup');
            var token = result.credential.accessToken;
            var user = result.user;
            window.location.href = "index.html";
        }).catch(function (error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            var email = error.email;
            var credential = error.credential;
            create_alert('error',error.message);
        });
    });

}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};