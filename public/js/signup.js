function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var userID = document.getElementById('userID');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    var btnFacebook = document.getElementById('btnfacebook');

    btnSignUp.addEventListener('click', function () {        
        var email = txtEmail.value;
        var password = txtPassword.value;
        var userid = userID.value;
        firebase.auth().createUserWithEmailAndPassword(email, password).then(e =>{
            var user = firebase.auth().currentUser;
            console.log(user);
            user.updateProfile({
                displayName : userid,
                photoURL : "https://firebasestorage.googleapis.com/v0/b/software-studio-2018-chia.appspot.com/o/images%2Fdefault.jpg?alt=media&token=ffea13bf-f113-4587-bef7-4ff7dc8d1291"
            });
            document.getElementById('inputEmail').value = '';
            document.getElementById('inputPassword').value = '';
            document.getElementById('userID').value = '';
            setTimeout(function(){
                window.location.href = "index.html";
            }, 1500); 
        }).catch(e => {
            create_alert('error',e.message);
            document.getElementById('inputEmail').value = '';
            document.getElementById('inputPassword').value = '';
            document.getElementById('userID').value = '';
        });
    });


    btnGoogle.addEventListener('click', e => {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location.href="index.html";
        }).catch(function (error) {
        create_alert('error',error.message);
    });
    });

    btnFacebook.addEventListener('click', e => {
        var facebook_provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(facebook_provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location.href = "index.html";
        }).catch(function (error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            var email = error.email;
            var credential = error.credential;
            create_alert('error',error.message);
        });
    });
    
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};